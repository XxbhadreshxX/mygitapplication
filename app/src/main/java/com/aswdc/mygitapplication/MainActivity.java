package com.aswdc.mygitapplication;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.mygitapplication.util.Const;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etPhoneNumber, etEmail;
    ImageView ivClose;
    TextView tvDisplay;
    Button btnSubmit;
    ImageView ivBackground;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbCricket;
    CheckBox chbFootBall;
    CheckBox chbHockey;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("TextViewValue", tvDisplay.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReference();
        intViewEvent();
        setTypefaceOnView();
        handleSavedInstance(savedInstanceState);
    }

    void handleSavedInstance(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            tvDisplay.setText(savedInstanceState.getString("TextViewValue"));
        }
    }

    void setTypefaceOnView() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");
        btnSubmit.setTypeface(typeface);
        tvDisplay.setTypeface(typeface);
        etFirstName.setTypeface(typeface);
        etPhoneNumber.setTypeface(typeface);
    }

    void intViewEvent() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.FIRST_NAME, etFirstName.getText().toString());
                    map.put(Const.LAST_NAME, etPhoneNumber.getText().toString());
                    map.put(Const.EMAIL_ADDRESS, etEmail.getText().toString());
                    map.put(Const.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());
                    String hobbies = "";
                    if (chbCricket.isChecked()) {
                        hobbies += "," + chbCricket.getText().toString();
                    }
                    if (chbFootBall.isChecked()) {
                        hobbies += "," + chbFootBall.getText().toString();
                    }
                    if (chbHockey.isChecked()) {
                        hobbies += "," + chbHockey.getText().toString();
                    }
                    map.put(Const.HOBBY, hobbies);
                    userList.add(map);

                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    intent.putExtra("UserList", userList);
                    startActivity(intent);
                }
            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tvDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                } else if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootBall.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.GONE);
                }
            }
        });
    }

    void initViewReference() {
        etFirstName = findViewById(R.id.etActFirstName);
        etPhoneNumber = findViewById(R.id.etActPhoneNumber);
        etEmail = findViewById(R.id.etActEmail);
        ivClose = findViewById(R.id.ivActClose);
        tvDisplay = findViewById(R.id.tvActDisplay);
        btnSubmit = findViewById(R.id.btnActSubmit);
        ivBackground = findViewById(R.id.ivActBackground);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbFootBall = findViewById(R.id.chbActFootBall);
        chbCricket = findViewById(R.id.chbActCricket);
        chbHockey = findViewById(R.id.chbActHockey);

        etPhoneNumber.setText("+91");
    }

    boolean isValid() {
        boolean flag = true;
        if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
            etFirstName.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String name = etFirstName.getText().toString();
            String namePattern = "[a-zA-Z]";
            if (!name.matches(namePattern)) {
                etFirstName.setError("Enter Valid Name");
                flag = false;
            }
        }

        //Phone Number Validation
        if (TextUtils.isEmpty(etPhoneNumber.getText())) {
            etPhoneNumber.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length() < 10) {
                etPhoneNumber.setError("Enter Valid Phone Number");
                flag = false;
            }
        }

        //Email Address Validation
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.error_enter_value));
            flag = false;
        } else {
            String email = etEmail.getText().toString();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!email.matches(emailPattern)) {
                etEmail.setError("Enter Valid Email Address");
                flag = false;
            }
        }

        //CheckBox Validation
        if ((!chbCricket.isChecked() && !chbFootBall.isChecked() && !chbHockey.isChecked())) {
            Toast.makeText(this, "Please Select any one Checkbox", Toast.LENGTH_LONG).show();
            flag = false;
        }

        return flag;
    }
}